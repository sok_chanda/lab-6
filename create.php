<?php

    include "connection.php";

    if(isset($_POST["submit"])){

        var_dump($_POST);

        $phone_number = $_POST['phone_number'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $url_fb = $_POST['url_fb_socail'];
        $profile_picture = $_POST['profile_picture'];

        $sql = "INSERT INTO contact(phone_number,first_name,last_name,url_fb_socail,profile_picture) VALUES(?,?,?,?,?)";
        $stmt = $con->prepare($sql);
        $stmt->bind_param("sssss",$phone_number,$first_name,$last_name,$url_fb,$profile_picture);
        $stmt= $stmt->execute();

        header("location: index.php");

    }
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
</head>
<body>
    <div class="container">
        <form action="" method="post" enctype="multipart/form-data" role="form">
            
            <div class="mb-3">
                <label for="" class="form-label">Phone Number</label>
                <input
                    type="text"
                    class="form-control"
                    name="phone_number"
                    id=""
                    aria-describedby="emailHelpId"
                    placeholder=""
                    require
                />
            </div>

            <div class="mb-3">
                <label for="" class="form-label">First Name</label>
                <input
                    type="text"
                    class="form-control"
                    name="first_name"
                    id=""
                    aria-describedby="emailHelpId"
                    placeholder=""
                    require
                />
            </div>

            <div class="mb-3">
                <label for="" class="form-label">Last Name</label>
                <input
                    type="text"
                    class="form-control"
                    name="last_name"
                    id=""
                    aria-describedby="emailHelpId"
                    placeholder=""
                    require
                />
            </div>

            <div class="mb-3">
                <label for="" class="form-label">URL Facebook</label>
                <input
                    type="text"
                    class="form-control"
                    name="url_fb_socail"
                    id=""
                    aria-describedby="emailHelpId"
                    placeholder=""
                    require
                />
            </div>

            <div class="mb-3">
                <label for="" class="form-label">Profile Picture</label>
                <input
                    type="text"
                    class="form-control"
                    name="profile_picture"
                    id=""
                    aria-describedby="emailHelpId"
                    placeholder=""
                    require
                />
            </div>

            <div class="mb-3">
                <input type="submit" name="submit" class="btn btn-primary" value="UPLOAD">
            </div>
            
        </form>
    </div>
</body>
</html>