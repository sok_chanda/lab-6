<?php

global $con;
include "connection.php";


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
    <style>
        body{
            font-family: Bahnschrift;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="col-12">
        <h1 class="text-center mb-3 mt-5">CRUD Function</h1>
    </div>
    <a href="create.php" class="btn btn-primary text-center">Add new</a>
    <?php
    $sql_get_all = "SELECT * FROM contact";
    $result = $con->query($sql_get_all);

    echo '<div class="table-responsive">
    <table class="table table-striped">
        <thead class="thead-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Phone Number</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">URL Facebook</th>
            <th scope="col">Profile Picture</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>';

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $id = $row["id"];
            echo '<tr>
                <td>' . $row["id"] . '</td>
                <td>' . $row["phone_number"] . '</td>
                <td>' . $row["first_name"] . '</td>
                <td>' . $row["last_name"] . '</td>
                <td><a href="' . $row["url_fb_socail"] . '" target="_blank" class="btn btn-primary btn-sm">View Facebook</a></td>
                <td><img src="' . $row["profile_picture"] . '" class="img-thumbnail" height="50" width="50"></td>
                <td><a href="update.php?Id='.$id.'" class="btn btn-success btn-sm">Edit</a></td>
                <td><a href="delete.php?Id='.$id.'" class="btn btn-danger btn-sm">Delete</a></td>
            </tr>';
        }
    } else {
        echo '<tr><td colspan="5" class="text-center">No records found</td></tr>';
    }

    echo '</tbody></table></div>';
    ?>
    
</div>
</body>
</html>