<?php
    include("connection.php");
    $id = $_GET['Id'];
    $sql = "SELECT * FROM contact";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    $first_name = $row['first_name'];
    $last_name = $row['last_name'];
    $phone_number = $row['phone_number'];
    $url_fb = $row['url_fb_socail'];
    $profile_picture = $row['profile_picture'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
</head>
<body>
    <div class="container">
        
        <form action="#" method="POST" role="form">
            <legend>Update User</legend>
            <?php 
            echo"
            <div class='form-group'>
                <label>ID</label>
                <input type='text' class='form-control' name='id' value='$id' required>
            </div>";
            ?>
            <div class="form-group">
                <label for="">Phone Number</label>
                <input type="text" class="form-control" id="" name="phone_number" value="<?= $phone_number?>" required>
            </div>
            <div class="form-group">
                <label for="">First Name</label>
                <input type="text" class="form-control" id="" name="first_name" placeholder="...." value="<?= $first_name?>"  required>
            </div>
            <div class="form-group">
                <label for="">Last Name</label>
                <input type="text" class="form-control" name="last_name" id="" placeholder="...." value="<?= $last_name?>"  required>
            </div>
            <div class="form-group">
                <label for="">Facebook</label>
                <input type="text" class="form-control" name="url_fb_socail" id="" placeholder="Facebook Link" value="<?= $url_fb?>"  required>
            </div>
            <div class="form-group">
                <label for="">Profile</label>
                <input type="text" class="form-control" name="profile_picture" id="" placeholder="Insert Your image link" value="<?= $profile_picture?>"  required>
            </div>
            <button type="submit" class="btn btn-primary" name="submit">Submit</button>
        </form>
        
        <a class="btn btn-default" href="index.php" role="button">Back</a>
        
        
    </div>
    <?php
    
        if(isset($_POST["submit"])){
            $id = $_POST['id'];
            $phone = $_POST['phone_number'];
            $first_name = $_POST['first_name'];
            $last_name = $_POST['last_name'];
            $fb = $_POST['url_fb_socail'];
            $profile = $_POST['profile_picture'];
            $sql = "UPDATE contact SET phone_number = '$phone',first_name = '$first_name',last_name = '$last_name',url_fb_socail = '$fb',profile_picture = '$profile'    WHERE id = '$id'";
            if(mysqli_query($con,$sql)){
                echo "<script> alert('Update Successfully')</script>";
                header("location: index.php");
            }else{
                echo "<script> alert('Update didn't Successfully')</script>";
            }
            mysqli_close($con);
        }
    ?>
</body>
</html>